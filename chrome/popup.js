"use strict";

////////////////////////////////////////////////////////////////////////////////
/// VALUES
////////////////////////////////////////////////////////////////////////////////

//////////////////////////////////////////////////
/// CONST
//////////////////////////////////////////////////
const	WH_DATACENTER		= 7.20 * 0.00000001;
const	WH_NETWORK_WIFI		= 4.29 * 0.0000001;
const	WH_NETWORK_WIRED	= 1.52 * 0.0000001;
const	WH_NETWORK_MOBILE	= 8.84 * 0.0000001;
const	WH_CO2				= 0.519;
const	DAYS				= ["sun", "mon", "tue", "wed", "thu", "fri", "sat"];

//////////////////////////////////////////////////
/// GLOBALS
//////////////////////////////////////////////////

let		g_table				= null;
let		g_graph				= null;
let		g_week				= null;

////////////////////////////////////////////////////////////////////////////////
/// FUNCTIONS
////////////////////////////////////////////////////////////////////////////////

//////////////////////////////////////////////////
/// STATS
//////////////////////////////////////////////////

function		get_unit(result, value, divider) {
	let	k, g, m;

	k = value / divider;
	/// NO
	if (k < 1) {
		result.value = value;	
		result.unit = "";
	} else {
		m = k / divider;
		/// KILO
		if (m < 1) {
			result.value = k;
			result.unit = "K";
		} else {
			g = m / divider;
			/// MEGA
			if (g < 1) {
				result.value = m;
				result.unit = "M";
			/// GIGA
			} else {
				result.value = g;
				result.unit = "G";
			}
		}
	}
	return result;
}

function		get_readable_value(value) {
	let	i;
	let	result;

	/// ZERO
	if (value == 0) {
		return 0;
	/// ROUND
	} else if (Math.floor(value) == value) {
		return Math.floor(value);
	}
	/// GET COMMA
	i = 1;
	while ((result = value.toFixed(i)) == 0 && i < 5) {
		++i;
	}
	/// IF TOO FARE
	if (i == 5 && result == 0) {
		return 0;
	}
	/// RETURN
	return value.toFixed(i);
}

function		update_stats_row(row, bytes) {
	let	wh;
	let	number;

	number = {};
	/// BYTES
	get_unit(number, bytes, 1024);
	g_table.bytes[row].value.textContent = get_readable_value(number.value);
	g_table.bytes[row].unit.textContent = number.unit + "B";
	/// WH
	wh = bytes * (WH_DATACENTER + WH_NETWORK_WIFI);
	get_unit(number, wh, 1000);
	g_table.wh[row].value.textContent = get_readable_value(number.value);
	g_table.wh[row].unit.textContent = number.unit + "Wh";
	/// CO2
	get_unit(number, wh * WH_CO2, 1000);
	g_table.co2[row].value.textContent = get_readable_value(number.value);
	g_table.co2[row].unit.textContent = number.unit + "gCO2";
}

function	create_stats_cell(row, type) {
	let	cell;
	let	value;
	let	unit;

	/// NUMBER
	value = document.createElement("span");
	value.setAttribute("class", "value");
	value.innerHTML = "0";
	/// UNIT
	unit = document.createElement("span");
	unit.setAttribute("class", "unit");
	unit.innerHTML = type;
	/// TABLE DATA
	cell = document.createElement("td");
	cell.appendChild(value);
	cell.appendChild(unit);
	/// UPDATE ROW
	row.appendChild(cell);
	/// RETURN CELL
	return {
		"value": value,
		"unit": unit
	};
}

function	create_stats_row(table, type) {
	let	row;
	let	site, today, week;

	/// CREATE ROW
	row = document.createElement("tr");
	/// CREATE CELLS
	site = create_stats_cell(row, type);
	today = create_stats_cell(row, type);
	week = create_stats_cell(row, type);
	/// UPDATE TABLE
	table.appendChild(row);
	/// RETURN ROW
	return {
		"site": site,
		"today": today,
		"week": week
	};
}

function	create_stats_table() {
	let	table;
	let	bytes, wh, co2;

	/// GET TABLE
	table = document.getElementById("stats");
	/// CREATE ROWS
	bytes = create_stats_row(table, "bytes");
	wh = create_stats_row(table, "wh");
	co2 = create_stats_row(table, "co2");
	/// SAVE TABLE
	g_table = {
		"bytes": bytes,
		"wh": wh,
		"co2": co2
	};
}

//////////////////////////////////////////////////
/// GRAPH
//////////////////////////////////////////////////

function	update_graph() {
	let	i;
	let	max;
	let	percent;

	/// GET MAX
	max = 0;
	for (i = 0; i <= 6; ++i) {
		max = Math.max(max, g_week[i]);
	}
	/// UPDATE GRAPH
	for (i = 0; i <= 6; ++i) {
		percent = ((100 * g_week[i]) / max) + "%";
		g_graph[i].style.background = "linear-gradient(0deg,#000 "
		+ percent + ",#fff " + percent + ")";
	}
}

function	create_bar(graph, week_day) {
	let	row;
	let	bar;
	let	day;

	/// BAR
	bar = document.createElement("div");
	bar.setAttribute("class", "graph_bar");
	/// DAY
	day = document.createElement("div");
	day.setAttribute("class", "graph_day");
	day.textContent = week_day;
	/// ROW
	row = document.createElement("div");
	row.setAttribute("class", "graph_row");
	row.appendChild(bar);
	//row.appendChild(value);
	row.appendChild(day);
	/// ADD ROW TO GRAPH
	graph.appendChild(row);
	g_graph.push(bar);
	return bar;
}

function	create_graph() {
	let	graph;
	let	i;

	graph = document.getElementById("graph");
	g_graph = [];
	for (i = 0; i <= 6; ++i) {
		create_bar(graph, DAYS[i]);
	}
}

//////////////////////////////////////////////////
/// GLOBAL
//////////////////////////////////////////////////

function	get_readable_storage(struct) {
	let	key;

	for (key in struct) {
		struct[key] = struct[key].newValue;
	}
	return (struct);
}

function	update_week_stats(data) {
	let	date;
	let	date_limit;
	let	list;
	let	i;
	let	day;

	/// GET WEEK LIMIT DAY
	date = new Date();
	date = new Date(date.getFullYear(), date.getMonth(), date.getDate());
	date_limit = date.getTime() - 518400000;
	/// BUILD LIST
	if (g_week == null) {
		g_week = [0, 0, 0, 0, 0, 0, 0];
	}
	for (i = 0; i <= 6; ++i) {
		day = data["d" + i];
		if (day && day.date >= date_limit) {
			g_week[i] = day.bytes;
		}
	}
}

function		get_day_bytes(today, time) {
	if (today == null || today.date != time) {
		return (0);
	}
	return (today.bytes);
}

function		update_page(data) {
	let	now;
	let	time;
	let	site_bytes;
	let	today_bytes;
	let	week_bytes;
	let	i;

	/// GET TIME
	now = new Date();
	now = new Date(now.getFullYear(), now.getMonth(), now.getDate());
	time = now.getTime();
	/// GET STATS
	update_week_stats(data);
	site_bytes = data.site ? data.site : 0;
	today_bytes = get_day_bytes(data.today, time);
	week_bytes = 0;
	for (i = 0; i <= 6; ++i) {
		week_bytes += g_week[i];
	}
	/// UPDATE PAGE
	update_stats_row("site", site_bytes);
	update_stats_row("today", today_bytes);
	update_stats_row("week", week_bytes);
	update_graph();
}

function		open_dashboard() {
	chrome.tabs.create({"url": "dashboard.html"}, null);
}

////////////////////////////////////////////////////////////////////////////////
/// RUN
////////////////////////////////////////////////////////////////////////////////

document.addEventListener('DOMContentLoaded', function() {
	let	b_more;

	/// GET ELEMENTS
	b_more = document.getElementById("b_more");
	/// FILL HTML
	create_stats_table();
	create_graph();
	/// PRINT
	chrome.storage.local.get(
	["site", "today", "d0", "d1", "d2", "d3", "d4", "d5", "d6"], update_page);
	/// SET EVENT LISTENER
	chrome.storage.onChanged.addListener(function (changes) {
		update_page(get_readable_storage(changes));
	});
	/// OPEN MAIN PAGE WITH BUTTON
	b_more.addEventListener("click", open_dashboard);
});
