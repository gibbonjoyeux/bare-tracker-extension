"use strict";

////////////////////////////////////////////////////////////////////////////////
/// VALUES
////////////////////////////////////////////////////////////////////////////////
const	WH_DATACENTER		= 7.20 * 0.00000001;
const	WH_NETWORK_WIFI		= 4.29 * 0.0000001;
const	WH_NETWORK_WIRED	= 1.52 * 0.0000001;
const	WH_NETWORK_MOBILE	= 8.84 * 0.0000001;
const	WH_CO2				= 0.519;
let		g_bytes				= null;
let		g_bytes_unit		= null;
let		g_wh				= null;
let		g_wh_unit			= null;
let		g_co2				= null;
let		g_co2_unit			= null;

////////////////////////////////////////////////////////////////////////////////
/// FUNCTIONS
////////////////////////////////////////////////////////////////////////////////

function		get_unit(result, value, divider) {
	let	k, g, m;

	k = value / divider;
	/// NO
	if (k < 1) {
		result.value = value;	
		result.unit = "";
	} else {
		m = k / divider;
		/// KILO
		if (m < 1) {
			result.value = k;
			result.unit = "K";
		} else {
			g = m / divider;
			/// MEGA
			if (g < 1) {
				result.value = m;
				result.unit = "M";
			/// GIGA
			} else {
				result.value = g;
				result.unit = "G";
			}
		}
	}
	return result;
}

function		update_page(bytes) {
	if (g_bytes != null) {
		let	wh;
		let	result = {};
		
		/// BYTES
		get_unit(result, bytes, 1024);
		g_bytes.textContent = result.value.toFixed(1);
		g_bytes_unit.textContent = result.unit + "B";
		/// WH
		wh = bytes * (WH_DATACENTER + WH_NETWORK_WIFI);
		get_unit(result, wh, 1000);
		g_wh.textContent = result.value.toFixed(1);
		g_wh_unit.textContent = result.unit + "Wh";
		/// CO2
		get_unit(result, wh * WH_CO2, 1000);
		g_co2.textContent = result.value.toFixed(1);
		g_co2_unit.textContent = result.unit + "gCO2";
	}
}

////////////////////////////////////////////////////////////////////////////////
/// RUN
////////////////////////////////////////////////////////////////////////////////

document.addEventListener('DOMContentLoaded', function() {
	/// GET ELEMENTS
	g_bytes = document.getElementById("bytes");
	g_bytes_unit = document.getElementById("bytes_unit");
	g_wh = document.getElementById("wh");
	g_wh_unit = document.getElementById("wh_unit");
	g_co2 = document.getElementById("co2");
	g_co2_unit = document.getElementById("co2_unit");
	/// PRINT
	chrome.storage.local.get(["today"], function (result) {
		if (result.today) {
			update_page(result.today.bytes);
		}
	});
	/// SET EVENT LISTENER
	//chrome.storage.onChanged.addListener(function (changed, area) {
	//	if (area == "local" && changed.today) {
	//		update_page(changed.today.newValue.bytes);
	//	}
	//});
});
