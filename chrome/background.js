
////////////////////////////////////////////////////////////////////////////////
/// FUNCTIONS
////////////////////////////////////////////////////////////////////////////////

//////////////////////////////////////////////////
/// TABS ACTIVATION
//////////////////////////////////////////////////

function		on_activated(info) {
	chrome.storage.local.set({"tab": info.tabId, "site": 0});
	chrome.tabs.sendMessage(info.tabId, {});
}

//////////////////////////////////////////////////
/// TABS COMMUNICATION
//////////////////////////////////////////////////

function		get_week(now) {
	let	start;

	start = new Date(now.getFullYear(), 0, 1);
	return (Math.ceil((((now - start) / 86400000) + start.getDay() + 1) / 7));
}

function		on_connection(port) {
	port.onMessage.addListener(function (message) {
		let	now;
		let	time;
		let	fmt_day, fmt_week, fmt_year;

		/// HANDLE DATE
		now = new Date();
		time = new Date(now.getFullYear(), now.getMonth(), now.getDate())
		.getTime();
		fmt_day = "d" + now.getDay();
		fmt_week = "w" + get_week(now);
		fmt_year = now.getFullYear();
		/// GET STORAGE
		chrome.storage.local.get(["tab", "today", fmt_day, fmt_week],
		function (data) {
			let	today;
			let	day;
			let	week;

			/// HANDLE SITE
			if (port.sender.tab.id == data.tab) {
				data.site = message.bytes_total;
			}
			/// HANDLE CURRENT DAY
			today = data.today;
			if (today == null) {
				today = { "date": time, "bytes": message.bytes_total };
			} else if (today.date == time) {
				today.bytes += message.bytes_diff;
			} else {
				today.date = time;
				today.bytes = message.bytes_diff;
			}
			data.today = today;
			/// HANDLE WEEK DAY
			day = data[fmt_day];
			if (day == null) {
				day = { "date": time, "bytes": message.bytes_diff };
			} else if (day.date == time) {
				day.bytes += message.bytes_diff;
			} else {
				day.date = time;
				day.bytes = message.bytes_diff;
			}
			data[fmt_day] = day;
			/// HANDLE YEAR WEEK
			week = data[fmt_week];
			if (week == null) {
				week = { "year": fmt_year, "bytes": message.bytes_diff };
			} else if (week.year == fmt_year) {
				week.bytes += message.bytes_diff;
			} else {
				week.year = fmt_year;
				week.bytes = message.bytes_diff;
			}
			data[fmt_week] = week;
			/// SET STORAGE
			chrome.storage.local.set(data, null);
		});
	});
}

////////////////////////////////////////////////////////////////////////////////
/// RUN
////////////////////////////////////////////////////////////////////////////////

/// HANDLE PORT CONNECTION
chrome.runtime.onConnect.addListener(on_connection);
/// HANDLE TAB ACTIVATION
chrome.tabs.onActivated.addListener(on_activated);
