"use strict";

////////////////////////////////////////////////////////////////////////////////
/// VALUES
////////////////////////////////////////////////////////////////////////////////

let		g_bytes				= 0;
let		g_port				= null;

let		g_state_timeout		= 5000;
let		g_state_count		= 0;

////////////////////////////////////////////////////////////////////////////////
/// FUNCTIONS
////////////////////////////////////////////////////////////////////////////////

function		send_status() {
	g_port.postMessage({
		"bytes_total": g_bytes,
		"bytes_diff": 0
	});
	/// RE-LAUNCH
	if (g_state_timeout == 0) {
		g_state_count = 0;
		g_state_timeout = 60000;
		load_resources();
	}
}

function		run_timeout(diff) {
	/// ACTIF
	if (diff > 0) {
		g_state_timeout = 5000;
		g_state_count = 0;
	// PASSIF
	} else {
		g_state_count++;
		/// 5s -> 30s
		if (g_state_timeout == 5000) {
			if (g_state_count == 6) {
				g_state_count = 0;
				g_state_timeout = 10000;
			}
		/// 10s -> 1m30
		} else if (g_state_timeout == 10000) {
			if (g_state_count == 6) {
				g_state_count = 0;
				g_state_timeout = 30000;
			}
		/// 30s -> 5m
		} else if (g_state_timeout == 30000) {
			if (g_state_count == 7) {
				g_state_count = 0;
				g_state_timeout = 60000;
			}
		/// 1m -> 10min
		} else if (g_state_timeout == 60000) {
			if (g_state_count == 5) {
				g_state_count = 0;
				g_state_timeout = 300000;
			}
		/// 5m -> 30min
		} else if (g_state_timeout == 300000) {
			if (g_state_count == 4) {
				g_state_timeout = 0;
				return;
			}
		}
	}
	/// RUN TIMEOUT
	setTimeout(load_resources, g_state_timeout);
}

function		load_resources() {
	let	i;
	let	resources;
	let	bytes;
	let	bytes_diff;

	/// LOAD RESOURCES
	resources = performance.getEntriesByType("resource");
	bytes = 0;
	/// LOOP RESOURCES
	for (i = 0; i < resources.length; i++) {
		if ("transferSize" in resources[i]) {
			bytes += resources[i].transferSize;
		}
	}
	/// HANDLE DIFF
	bytes_diff = bytes - g_bytes;
	g_bytes = bytes;
	if (bytes_diff > 0) {
		g_port.postMessage({
			"bytes_total": bytes,
			"bytes_diff": bytes_diff
		});
	}
	/// HANDLE TIMEOUT
	run_timeout(bytes_diff);
}

////////////////////////////////////////////////////////////////////////////////
/// RUN
////////////////////////////////////////////////////////////////////////////////

/// PORT
g_port = chrome.runtime.connect();
/// RUN EVERY 5 sec
setTimeout(load_resources, g_state_timeout);
/// ON MESSAGE
chrome.runtime.onMessage.addListener(send_status);
